# redridge-test-task-frontend

## Prerequisites
- [node.js](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)
- installed and runned [backend](https://bitbucket.org/YuraMR/redridge-test-task-backend)

## Get Started

1. Install dependencies:
    ```bash
    yarn install
    ```

2. Run in development mode:
    ```bash
    yarn start
    ```

3. Production build process:
    ```bash
    yarn build
    ```
