import React, { useEffect, useReducer } from "react";
import './App.css';
import {getQuestions, postPollTemplate} from "./utils/api"
import {createDragEndHandler} from "./utils/auxiliary";
import {DragDropContext} from "react-beautiful-dnd";
import EnhancedDroppable from "./components/EnhancedDroppable";

const reducer = (state, action) => {
  const { data, type } = action;

  switch (type) {
    case 'questions':
      return {...state, questions: data};
    case 'selectedQuestions':
      return {...state, selectedQuestions: data};
    default:
      throw new Error();
  }
};

const createButtonProps = (questions, selectedQuestions) => ({ questions, selectedQuestions });

const App = () => {
  const [state, dispatch] = useReducer(reducer, {questions: [], selectedQuestions: []});

  const setQuestions = () => {
    getQuestions().then(data =>
      dispatch({type: 'questions', data})
    )
  };

  const handleDragEnd = createDragEndHandler(state, dispatch);

  const resetSelectedQuestions = data =>
    postPollTemplate(data).then(() =>
      dispatch({type: 'selectedQuestions', data: []})
    );

  const handlers = createButtonProps(setQuestions, resetSelectedQuestions);
  const labels = createButtonProps("Refresh question list", "Submit new poll template");

  const stateKeys = Object.keys(state);

  useEffect(setQuestions, {});

  return (
    <div className="App">
      <DragDropContext onDragEnd={handleDragEnd}>
        {stateKeys.map(item => (
          <EnhancedDroppable
            droppableId={item}
            items={state[item]}
            key={item}
          >
            <button onClick={() => handlers[item](state[item])}>{labels[item]}</button>
          </EnhancedDroppable>
        ))}
      </DragDropContext>
    </div>
  );
};

export default App;
