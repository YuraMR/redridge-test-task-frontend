import { compose } from "./common";

const capitalizeFirstLetter = ([first, ...rest]) => [first.toUpperCase(), ...rest].join('');

const capitalizeLettersAfterUnderscore = string => string.replace(/_([a-z])/g, letter =>
  letter.toUpperCase()
);

const removeUnderscore = string => string.replace(/_/g, ' ');

const makeReadable = compose(removeUnderscore, capitalizeLettersAfterUnderscore, capitalizeFirstLetter);

export { makeReadable }
