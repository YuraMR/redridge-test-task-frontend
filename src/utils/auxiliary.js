/**
 * Reorder items in one list
 */
const reorder = (list, startIndex, endIndex) => {
  const result = [...list];
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const createAction = (data, type) => ({data, type});

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = [...source];
  const destClone = [...destination];
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  return {
    destination: createAction(destClone, droppableDestination.droppableId),
    source: createAction(sourceClone, droppableSource.droppableId)
  }
};

const createDragEndHandler = (state, dispatch) => result => {
  const { source, destination } = result;

  if (!destination) {
    return;
  }

  const condition = source.droppableId === destination.droppableId;

  if (condition) {
    const { droppableId, index } = source;
    const data = reorder(
      state[droppableId],
      index,
      destination.index
    );

    dispatch({type: droppableId, data});
  } else {
    const result = move(
      state[source.droppableId],
      state[destination.droppableId],
      source,
      destination
    );

    dispatch(result.destination);
    dispatch(result.source);
  }
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? 'lightgreen' : 'grey',

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? 'lightblue' : 'lightgrey',
  padding: grid,
  minWidth: 250
});

export { createDragEndHandler, getItemStyle, getListStyle }
