const compose = (...functions) => value => functions.reduceRight(
  (result, item) => item(result), value
);

// const pipe = (...functions) => value => functions.reduce(
//   (result, item) => item(result), value
// );

export { compose }
