import axios from "axios";

const callApi = (method = 'get') => slug => data =>
  axios[method](`http://localhost:8080/api/${slug}`, data)
    .then(({ data }) => data);

const getData = callApi();
const getQuestions = getData('questions');

const postData = callApi('post');
const postPollTemplate = postData('pollTemplates');

export { getData, getQuestions, postPollTemplate }
