import React from "react";
import {makeReadable} from "../utils/stringTransformations";

const Label = ({ label }) => (
  <div>{makeReadable(label)}</div>
);

export default Label
