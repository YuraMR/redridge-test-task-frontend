import React from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import { getListStyle } from "../utils/auxiliary";
import Label from "./Label";
import DraggableChildren from "./DraggableChildren";

const EnhancedDroppable = ({ children, droppableId, items }) => (
  <Droppable droppableId={droppableId}>
    {(provided, snapshot) => (
      <div
        ref={provided.innerRef}
        style={getListStyle(snapshot.isDraggingOver)}>
        <Label label={droppableId}/>
        {children}
        {items.map((item, index) => (
          <Draggable
            draggableId={item.id}
            index={index}
            key={item.id}
          >
            {(provided, snapshot) => (
              <DraggableChildren
                item={item}
                provided={provided}
                snapshot={snapshot}
              />
            )}
          </Draggable>
        ))}
        {provided.placeholder}
      </div>
    )}
  </Droppable>
);

export default EnhancedDroppable
