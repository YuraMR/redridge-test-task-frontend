import React from "react";
import {getItemStyle} from "../utils/auxiliary";

const DraggableChildren = ({item, provided, snapshot}) => (
  <div
    ref={provided.innerRef}
    {...provided.draggableProps}
    {...provided.dragHandleProps}
    style={getItemStyle(
      snapshot.isDragging,
      provided.draggableProps.style
    )}>
    <div>{item.question}</div>
    <ul>
      {item.answers.map((answer, index) => (
        <li key={index}>{answer}</li>
      ))}
    </ul>
  </div>
);

export default DraggableChildren
